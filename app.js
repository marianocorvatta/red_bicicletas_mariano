require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
const jwt = require('jsonwebtoken');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/usuarios');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasApiRouter = require('./routes/api/bicicletas'); 
var usuariosApiRouter = require('./routes/api/usuarios'); 
var tokenRouter = require('./routes/token');
var authApiRouter = require('./routes/api/auth');

const Usuario = require('./models/usuario');
const Token = require ('./models/token');

const store = new session.MemoryStore;

var app = express();

app.set('secretKey', 'jwt_pwd_!!223344');

app.use(session({
  cookie: { maxAge: 240 * 60 *60 * 1000 },
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicis_ªª·"!·"!·!"·1465' 
}));

var mongoose = require('mongoose');
// var mongoDb = 'mongodb+srv://admin:IdYk0NyBGzAh4z90@red-bicicletas-mariano.vbuyu.mongodb.net/<dbname>?retryWrites=true&w=majority';
// si estoy en el ambiente de desarrollo usar 
// var mongoDb = 'mongodb://localhost/red_bicicletas';
// sino usar 
var mongoDb = process.env.MONGO_URI;
mongoose.connect(mongoDb, { useNewUrlParser: true,useUnifiedTopology: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDb connection error:'));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/login', function(req, res){
  res.render('session/login');
});

app.get('/login', function(req, res, next){
  passport.authenticate('local', function(err, usuario, info){
    if (err) return next(err);
    if (!usuario) return res.render('session/login', {info});
    req.logIn(usuario, function(err){
      if (err) return next(err);
      return res.redirect('/');
    })
  })(req, res, next);
});

app.get('/logout', function(req , res){
  req.logOut();
  res.redirect('/');
});

app.get('/forgotPassword', function(req, res){

});

app.post('/forgotPassword', function(req, res){

});

app.use('/', indexRouter); 
app.use('/token', tokenRouter);
app.use('/usuarios', usersRouter);
app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasApiRouter);
app.use('/api/usuarios', usuariosApiRouter);
app.use('/api/auth', authApiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next){
  if (req.user){
    next();
  } else {
    console.log('user sin loguearse');
    res.redirect('/login');
  }
};

function validarUsuario(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded){
    if (err) {
      res.json({ status: "error", message: err.message, data: null});
    } else {
      req.body.userId = decoded.id;
      console.log('jwt verify:' + decoded);
      next();
    }
  });
}




module.exports = app;


